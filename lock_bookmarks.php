<?php
/* @update: 2021JUL19 */
if (isset($_POST['locked']) and $_POST['locked'] === "1"):

    $enc_msg = '';
    $unlink_pf_msg = '';
    $unlink_fk_msg = '';

    // search for the key pairs files.
    $path_k = 'func/keyup/';
    $files_k = array_diff(scandir($path_k), array('.', '..'));
    #print_r($files_k);

    // search for the database file.
    $path_d = 'db/';
    $files_d = array_diff(scandir($path_d), array('.', '..'));
    #print_r($files_d);

    ####################
    ##### ENCRYPT ######
    ####################
    // name public key
    foreach($files_k as $fk):
        $xp=explode('.',$fk);
        if ($xp['1']==="pem"):
            $pubkey='func/keyup/'.$xp['0'].'.'.$xp['1'];
            $public_key=openssl_pkey_get_public(file_get_contents($pubkey)); // sample: pubkey.pem or public.pem
        endif;
    endforeach;

    // name encrypted file
    foreach($files_d as $fd):
        $xpd=explode('.',$fd);
        if ($xpd['1']==="dec"):
            $cipherfile='db/'.$xpd['0'].'.'.$xpd['2'].'.'.'enc';
            $plainfile='db/'.$xpd['0'].'.'.$xpd['1'].'.'.$xpd['2']; // we use the file as it is
        endif;
    endforeach;

    // generate random aes encryption key
    $key = openssl_random_pseudo_bytes(32); // 32 bytes = 256 bit aes key

    // now encrypt the aes encryption key with the public key ($encryptedKey will hold the result of the encryption)
    openssl_public_encrypt($key,$encryptedKey,$public_key,OPENSSL_PKCS1_OAEP_PADDING);

    // save 256 bytes long encrypted key
    file_put_contents($cipherfile,$encryptedKey);

    // aes cbc encryption
    // generate random iv
    $iv = openssl_random_pseudo_bytes(16);

    // save 16 bytes long iv
    file_put_contents($cipherfile,$iv,FILE_APPEND);
    $data = file_get_contents($plainfile);
    $cipher = openssl_encrypt($data,'AES-256-CBC',$key,OPENSSL_RAW_DATA,$iv);

    // save cipher
    file_put_contents($cipherfile,$cipher,FILE_APPEND);
    $enc_msg = 'Encryption Finished' . PHP_EOL;

    // delete unencrypted database.
    if (!unlink($plainfile)):
        $unlink_pf_msg = "The unencrypted database file `$plainfile` cannot be deleted due to an error.";
    else:
        $unlink_pf_msg = "The unencrypted database file `$plainfile` has been deleted.";
    endif;

    // delete all key pairs.
    foreach($files_k as $fk):
        if (!unlink('func/keyup/'.$fk)):
            $unlink_fk_msg .= "The key `$fk` cannot be deleted due to an error.<br>";
        else:
            $unlink_fk_msg .= "The key `$fk` has been deleted.<br>";
        endif;
    endforeach;

    // display messages and actions
    echo '
        <div class="alert alert-warning alert-dismissible fade show" role="alert">
          ' . $enc_msg . '<br>
          ' . $unlink_pf_msg . '<br>
          ' . $unlink_fk_msg . '<br>
          <strong>Warning!</strong> The database was closed and encrypted.
          <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    ' . PHP_EOL;

    //destroy the session
    session_start();
    $_SESSION["lock_state"] = true;

endif;
