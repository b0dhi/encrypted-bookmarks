# Encrypted Bookmarks

A simple encrypted bookmarks by "Break Free Team"

This bookmarks make use of standard encryption `AES-256-CBC`, we intend to change it in the future though.


# Database

The database used with this program is SQLite, however we offer the SQL scheme and it can be converted into any other database, we gave preference to SQLite for the simplistic way to store the data in a single file allowing the encryption to be worked on that file, with MySQL you probably would have to work with the `ibdata1` file.

A warning: If your bookmark inflate to gigas in size, this bookmarks will not work as expected, the ultimate idea is to make this little bookmarks portable to be carried out in any USB stick.

## Firefox and other Browsers

If you have a huge bookmark with FF or any other browser, it still possible to import all your data into `Encrypted Bookmarks`, in order to do it, will be necessary to first convert the FF JSON or HTML file into DB file readable by SQLite. I do recommend for now to use a tool called `buku` here is the link https://github.com/jarun/buku
You basically need to import your FF bookmark file into `buku` and export it into the DB SQLite format, it is a simple operation that takes seconds depending on size of your bookmark.

You should be asking now, why just not use `buku`? Simple, `buku` is a great command line bookmark tool and we offer a web and portable (soon) versions with a graphical interface, which make it useful for everyone.



# Operating Systems

## Linux

### Self-hosted version

`Encrypted Bookmarks` runs on any Linux system with PHP and Apache current versions installed.

### Portable version

Coming soon portable version.


## Windows 

### Self-hosted version

`Encrypted Bookmarks` runs on any Windows system with PHP and Apache current versions installed.

### Portable version

Coming soon portable version.


# Remarks

We offer no garantee at all, use this piece of software at your own risk. It is free for all, you do not have to link anything back to us, we do not want any sort of publicity.


# TODO

- Add new bookmark still pending.
- Fix warnings.
- Portable versions for Linux and Windows.
- Implement a better encryption solution, but for a bookmark probably the current solution still good.
- Generate key pairs via interface instead of command line.
- Upload interface for database file (DB file) instead of manually load the directory with it.


# Help

We do not provide support for this tool, but keep an eye on this repository, eventually we will update it.
If you want to chat with us, please access the following address in your Tor Browser

http://onionshare:contents-refreeze@h34tyuiur35ahuovld5ygkyowohavug34mbokxydiyp6vv37yxhyz3id.onion

