<?php
/* @update: 2021JUL15 */


####################
##### UPLOAD #######
####################

// file name
$filename = $_FILES['file']['name'];
$total_f  = count($_FILES['file']['name']);
//print '<pre>';var_dump($_FILES['file']);print '</pre>';


// Loop through each file
for( $i=0; $i < $total_f; $i++ ) {

  //Get the temp file path
  $tmpFilePath = $_FILES['file']['tmp_name'][$i];

  //Make sure we have a file path
  if ($tmpFilePath != "") {

    //Setup our new file path
    $newFilePath = "func/keyup/" . $_FILES['file']['name'][$i];

    //Upload the file into the temp dir
    if (move_uploaded_file($tmpFilePath, $newFilePath)) {

        $response[] = $newFilePath;

    }
  }
}
//print '<pre>';print_r($response);print '</pre>';




####################
##### DECRYPT ######
####################

if ( (isset($response['0']) and !empty($response['0'])) and (isset($response['1']) and !empty($response['1']) )):

    //echo 'PHP OpenSSL RSA & AES CBC 256 hybrid encryption' . PHP_EOL;
    $rsa_msg = 'PHP OpenSSL RSA (2048) & AES CBC 256 hybrid encryption. ' . PHP_EOL;

    // get filenames
    $plainfile     = "db/bookmarks.db";
    $cipherfile    = "db/bookmarks.db.enc";
    $decryptedfile = "db/bookmarks.dec.db";

    // private key pass phrase (optional)
    $passwd = 'Akram Crown Sesame Witch Poison'; // pass phrase for `privkey.pem`

    // load private & public key
    $public_key    = openssl_pkey_get_public(file_get_contents( $response['0'] ));  // pubkey.pem   public.pem
    $private_key   = openssl_pkey_get_private(file_get_contents( $response['1'] )); // ,$passwd    privkey.pem  private.key


    ### decryption ###
    // read the data
    $handle            = fopen($cipherfile, "rb");
    // read 256 bytes long encryptedKey
    $decryptionkeyLoad = fread($handle, 256);
    // decrypt the encrypted key with private key
    $z = openssl_private_decrypt($decryptionkeyLoad, $decrypted, $private_key, OPENSSL_PKCS1_OAEP_PADDING);
    
    // read 16 bytes long iv
    $ivLoad            = fread($handle, 16);
    // read ciphertext
    $dataLoad          = fread($handle, filesize($cipherfile));
    fclose($handle);
    $decrypt           = openssl_decrypt($dataLoad, 'AES-256-CBC', $decrypted, OPENSSL_RAW_DATA, $ivLoad);
    file_put_contents($decryptedfile, $decrypt);


    // remove the encrypted database.
    if (!unlink($cipherfile)) {
        $unlink_msg = "The encrypted database file `$cipherfile` cannot be deleted due to an error.";
    }
    else {
        $unlink_msg = "The encrypted database file `$cipherfile` has been deleted.";
    }

    // RENAME THE KEY PAIRS (it facilitates when encrypting back the database, 
    // allowing user to upload the files with any desired names)
    // @TODO 
    // bookmarks.dec.db  <---- unencrypted
    // bookmarks.db.enc  <---- encrypted
    

    // create a session to flag the database is open now.
    session_start();
    $_SESSION["lock_state"] = false;

    // display messages and actions
    echo '
        <div class="alert alert-warning alert-dismissible fade show" role="alert">
          ' . $rsa_msg . '<br>
          ' . $unlink_msg . '<br>
          <strong>Warning!</strong> The database was opened.
          <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    ' . PHP_EOL;

    // redirect/button to main interface.
    echo '
        <div class="btn-group text-right">
            <a href="index.php" class="btn btn-primary active">
                <span class="btn-label"><i class="fa fa-search"></i></span>&nbsp;&nbsp;Search Bookmarks
            </a>
        </div>
    ';

endif;










/* works for one single file 
// Location
$location = 'func/keyup/'.$filename;

// file extension
$file_extension = pathinfo($location, PATHINFO_EXTENSION);
$file_extension = strtolower($file_extension);

// Valid extensions
$image_ext = array("key","pem","asc","pck");

$response = 0;
if(in_array($file_extension,$image_ext)){
  // Upload file
  if(move_uploaded_file($_FILES['file']['tmp_name'],$location)){
    $response = $location;
  }
}
echo $response;
*/



/* if there is more inputs 
if (isset($_POST['key_file'])) {
	$key_file = strip_tags($_POST['key_file']);

	# passwd is not mandatory (decryption script needs adjustment. see todo list.)
	if (isset($_POST['pass_phrase']) and !empty($_POST['pass_phrase'])) {
		$pass_phrase = strip_tags($_POST['pass_phrase']);
	}
}
*/
?>
