<?php
// hybrid encryption
// https://stackoverflow.com/questions/64693606/php-encrypt-a-file-using-public-key-and-decrypt-using-private-key

// KEY GENERATION - options
// private key
// > openssl genpkey -aes-256-cbc -algorithm RSA -out privkey.pem -pkeyopt rsa_keygen_bits:4096
// public key
// > openssl rsa -in privkey.pem -out pubkey.pem -pubout
// ref.: 
//  https://stackoverflow.com/questions/4294689/how-to-generate-an-openssl-key-using-a-passphrase-from-the-command-line
//  https://www.php.net/manual/en/ref.sodium.php
//
// or
//
// RSA private key
// openssl genrsa -out private.key 2048
// public key
// openssl rsa -in private.key -out public.pem -outform PEM -pubout


if ( (isset($_GET['e']) and $_GET['e'] === '1') or (isset($_GET['d']) and $_GET['d'] === '1') ):

    //echo 'PHP OpenSSL RSA & AES CBC 256 hybrid encryption' . PHP_EOL;
    echo 'PHP OpenSSL RSA (4096) & AES CBC 256 hybrid encryption' . PHP_EOL;

    // get filenames
    $plainfile     = "../db/bookmarks_copy.db";
    $cipherfile    = "../db/bookmarks_copy.db.enc";
    $decryptedfile = "../db/bookmarks_copy_dec.db";

    // private key pass phrase
    $passwd = 'Akram Crown Sesame Witch Poison'; // $_GET['p'];  pass phrase for `privkey.pem`

    // load private & public key
    $public_key    = openssl_pkey_get_public(file_get_contents('public.pem')); // pubkey.pem   public.pem
    $private_key   = openssl_pkey_get_private(file_get_contents('private.key')); // , $passwd    privkey.pem  private.key

endif;



####################
##### ENCRYPT ######
####################
if (isset($_GET['e']) and $_GET['e'] === '1'):

    // generate random aes encryption key
    $key           = openssl_random_pseudo_bytes(32); // 32 bytes = 256 bit aes key

    // now encrypt the aes encryption key with the public key
    openssl_public_encrypt($key, $encryptedKey, $public_key, OPENSSL_PKCS1_OAEP_PADDING);

    // save 256 bytes long encrypted key
    file_put_contents($cipherfile, $encryptedKey);
    // aes cbc encryption
    // generate random iv
    $iv            = openssl_random_pseudo_bytes(16);
    // save 16 bytes long iv
    file_put_contents($cipherfile, $iv, FILE_APPEND);
    $data          = file_get_contents($plainfile);
    $cipher        = openssl_encrypt($data, 'AES-256-CBC', $key, OPENSSL_RAW_DATA, $iv);
    // save cipher
    file_put_contents($cipherfile, $cipher, FILE_APPEND);
    echo 'encryption finished' . PHP_EOL;

endif;




####################
##### DECRYPT ######
####################
if (isset($_GET['d']) and $_GET['d'] === '1'):

    // decryption
    // read the data
    $handle            = fopen($cipherfile, "rb");
    // read 256 bytes long encryptedKey
    $decryptionkeyLoad = fread($handle, 256);
    // decrypt the encrypted key with private key
    $z = openssl_private_decrypt($decryptionkeyLoad, $decrypted, $private_key, OPENSSL_PKCS1_OAEP_PADDING);
    
    // read 16 bytes long iv
    $ivLoad            = fread($handle, 16);
    // read ciphertext
    $dataLoad          = fread($handle, filesize($cipherfile));
    fclose($handle);
    $decrypt           = openssl_decrypt($dataLoad, 'AES-256-CBC', $decrypted, OPENSSL_RAW_DATA, $ivLoad);
    file_put_contents($decryptedfile, $decrypt);
    echo '<br>decryption finished' . PHP_EOL;


    // string check
    // string(81) "error:04099079:rsa routines:RSA_padding_check_PKCS1_OAEP_mgf1:oaep decoding error"
    /*
    print '<pre>';
    var_dump(openssl_error_string());
    var_dump($decrypt); 
    print '</pre>';
    */


endif;
?>