<?php
require 'bookmarks.php';

$pdo = (new SQLiteConnection())->connect();
if ($pdo != null)
    $conn_msg = 'Connected to the SQLite database successfully!';
else
    $conn_msg = ' {status: error, msg: could not connect to the SQLite database} ';





////////////////////// search //////////////////////
$type = 0;
if (isset($_POST['type'])) {
   $type = $_POST['type'];
}

// Search result
if ($type == 1) {
    #$searchText = mysqli_real_escape_string($con,$_POST['search']);
    $searchText = $_POST['search'];

    $sql = 'SELECT * FROM bookmarks WHERE metadata LIKE "%'.$searchText.'%" order by metadata ASC';

    $search_arr = [];
    foreach ($pdo->query($sql) as $row) {
        $id           = $row['id'];
        $url          = $row['URL'];
        $metadata     = $row['metadata'];
        $tags         = $row['tags'];
        $search_arr[] = [
            "id"       => $id, 
            "url"      => $url, 
            "metadata" => $metadata, 
            "tags"     => $tags
        ];
    }
    echo json_encode($search_arr);
}

// get User data
if ($type == 2) {
    #$userid = mysqli_real_escape_string($con,$_POST['userid']);
    $id = $_POST['id'];

    $sql = "SELECT * FROM bookmarks where id=".$id;

    $return_arr = array();

    foreach ($pdo->query($sql) as $row) {
        $id           = $row['id'];
        $url          = $row['URL'];
        $metadata     = $row['metadata'];
        $tags         =  $row['tags'];
        $return_arr[] = [
            "id"       => $id, 
            "url"      => $url, 
            "metadata" => $metadata, 
            "tags"     => $tags
        ];
    }
    echo json_encode($return_arr);
}