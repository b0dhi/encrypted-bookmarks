<?php
#require 'config.php';

class SQLiteConnection {
    /**
     * PDO instance
     * @var type 
     */
    private $pdo;

    /**
     * return in instance of the PDO object that connects to the SQLite database
     * @return \PDO
     */
    public function connect() {
        try {
            if ($this->pdo == null) {
                $this->pdo = new PDO("sqlite:db/bookmarks.dec.db","","",array(
                    PDO::ATTR_PERSISTENT => true
                ));
            }
            return $this->pdo;
        } catch(PDOException $e) {
            #logerror($e->getMessage(), "opendatabase");
            print "Error in open db " . $e->getMessage();
        }
    }
}


