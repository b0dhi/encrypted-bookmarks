<?php
session_start();
//$_SESSION["lock_state"] = true; // initiate locked

require 'bookmarks.php';
$pdo = (new SQLiteConnection())->connect();
if ($pdo != null) {
    $conn_msg = 'Connected to the SQLite database successfully!';
} else {
    $conn_msg = ' {status: error, msg: could not connect to the SQLite database} ';
}



// query all tags
$current_tags = [];
$sql_tags = 'SELECT tags FROM bookmarks GROUP BY tags ORDER BY tags ASC;';
foreach ($pdo->query($sql_tags) as $row_tags) {
    $current_tags[] = $row_tags['tags'];
}



// convert tags from array to json format
$imp_tags = implode(',', $current_tags);
$exp_tags = explode(',', $imp_tags);
$new_tags = [];
$countme  = 0;
foreach ($exp_tags as $et) {
    // remove undesired tags
    if (empty($et) or $et === '2021jul07') {
        // do nothing
    } else {
        $countme +=1;
        $new_tags[] = [ "value" => $countme, "text" => $et, "continent" => "Task" ];
    }
}
// convert array into json
$tagsJSON = json_encode($new_tags);



// query URL by tags
$r_url = [];
$ndx   = 0;
foreach ($new_tags as $tj) {

    $ndx +=1;
    $sql_url = 'SELECT * FROM bookmarks WHERE tags LIKE "%'.$tj['text'].'%"';
    $stmt = $pdo->prepare($sql_url);
    $stmt->execute();
    $row_url = $stmt->fetchAll();

    foreach ($row_url as $ru) {
        $r_url[] = [
            "tag" => $tj['text'],
            "url" => $ru['URL']
        ];
    }
}
?>

<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="A simple encrypted bookmarks.">
    <meta name="author" content="break free team">
    <meta name="generator" content="BFT 0.84.0">

    <!-- CSS only -->
    <link href="assets/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link href='assets/bootstrap-tagsinput.css' rel='stylesheet'>
    <!-- Font Awesome CSS -->
    <link href="assets/css/all.css" rel="stylesheet">
    <!-- <link href='assets/bootstrap-treeview.css' rel='stylesheet'> -->

    <style>
    body {
     background-image: url("assets/img/bg2.jpg"); /* no-repeat center center fixed */
     background-position: center;
     background-size: cover; /* cover contain 100% */
     background-repeat: no-repeat;
     height: 97%;
     -webkit-background-size: cover;
     -moz-background-size: cover;
     -o-background-size: cover;
    }
    .clear{
     clear:both;
     margin-top: 20px;
    }
    #searchResult{
     list-style: none;
     padding: 0px;
     width: 92%;
     position: relative;
     margin: 0;
    }
    #searchResult li{
     background: lavender;
     padding: 4px;
     margin-bottom: 1px;
    }
    #searchResult li:nth-child(even){
     background: cadetblue;
     color: white;
    }
    #searchResult li:hover{
     cursor: pointer;
    }
    input[type=text]{
     padding: 5px;
     width: 100%;
     letter-spacing: 1px;
    }
    /* autocomplete tagsinput*/
    .label-info {
      background-color: #5bc0de;
      display: inline-block;
      padding: 0.2em 0.6em 0.3em;
      font-size: 75%;
      font-weight: 700;
      line-height: 1;
      color: #fff;
      text-align: center;
      white-space: nowrap;
      vertical-align: baseline;
      border-radius: 0.25em;
    }
    footer {
      background-color: #0E1717;
      position: fixed;
      height: 20px;
      bottom: 0;
      width: 100%;
      font-weight: lighter;
      text-align: right;
    }
    </style>
</head>

<body>

    <!-- top bar navigation -->
    <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
      <div class="container-fluid">
        <span class="d-inline-block" tabindex="0" data-bs-toggle="popover" data-bs-trigger="hover focus" data-bs-html="true" data-bs-content="Encrypted Bookmarks<br/>v0.1 Using AES256">
            <a class="navbar-brand" href="index.php">BOOKMARKS</a>
        </span>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>


        <?php if (isset($_SESSION["lock_state"]) and $_SESSION["lock_state"] === true) { ?>

            <div class="collapse navbar-collapse" id="navbarCollapse">
              <ul class="navbar-nav me-auto mb-2 mb-md-0">
                <li class="nav-item">
                  <div class="nav-link active" aria-current="page" href="#">
                    <button type="button" class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#unlock">
                        <span class="btn-label"><i class="fa fa-unlock"></i></span>&nbsp;&nbsp;Unlock Database
                    </button>
                  </div>
                </li>
              </ul>
            </div>

        <?php } else { ?>

            <div class="collapse navbar-collapse" id="navbarCollapse">
              <ul class="navbar-nav me-auto mb-2 mb-md-0">
                <li class="nav-item">
                  <div class="nav-link active" aria-current="page" href="#">
                    <button type="button" class="btn btn-warning" data-bs-toggle="modal" data-bs-target="#addBookmark">
                        Add Bookmark
                    </button>&nbsp;
                    <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#listTags">
                        Tags
                    </button>&nbsp;
                    <button type="button" class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#lock">
                        <span class="btn-label"><i class="fa fa-lock"></i></span>&nbsp;&nbsp;Lock Database
                    </button>
                  </div>
                </li>
              </ul>
              <form class="d-flex">
                <input class="form-control me-2" type="search" id="txt_search" name="txt_search" placeholder="Search Metadata" aria-label="Search">
              </form>
            </div>

        <?php } ?>
        

      </div>
    </nav>



    <!-- add bookmark modal -->
    <div class="modal fade" id="addBookmark" tabindex="-1" aria-labelledby="modalLabel" aria-hidden="true">
      <div class="modal-dialog modal-fullscreen">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="modalLabel">Add Bookmark</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
            <form>
              <div class="mb-3">
                <label for="add-url" class="col-form-label">URL</label>
                <input type="text" class="form-control" name="frm_url" id="add-url">
              </div>
              <div class="mb-3">
                <label for="add-metadata" class="col-form-label">METADATA</label>
                <textarea class="form-control" name="frm_metadata" id="add-metadata"></textarea>
              </div>
              <div class="mb-3">
                <label for="add-tags" class="col-form-label">TAGS</label>
                <!--<textarea class="form-control" id="add-tags"></textarea>-->
                <input type="text" class="form-control" name="frm_tags[]" id="add-tags">
              </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary">Save changes</button>
          </div>
        </div>
      </div>
    </div>



    <!-- list tags modal -->
    <div class="modal fade" id="listTags" tabindex="-1" aria-labelledby="modalLabel" aria-hidden="true">
      <div class="modal-dialog modal-fullscreen">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="modalLabel">Bookmark Tags</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">


            <div class="col-sm-12">
                <h3>Tags</h3>
                <!-- <div id="tree"></div> -->

                <div class="accordion" id="accordionExample">

                    <?php $ndx = 0; foreach ($r_url as $r) { $ndx +=1; ?>

                          <div class="accordion-item">
                            <h2 class="accordion-header" id="heading-<?php echo $ndx; ?>">
                              <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse-<?php echo $ndx; ?>" aria-expanded="true" aria-controls="collapse-<?php echo $ndx; ?>">
                                <?php echo $r['tag']; ?>
                              </button>
                            </h2>
                            <div id="collapse-<?php echo $ndx; ?>" class="accordion-collapse collapse " aria-labelledby="heading-<?php echo $ndx; ?>" data-bs-parent="#accordionExample">
                              <div class="accordion-body">
                                <strong>[<?php echo $ndx; ?>]</strong> <a target="_blank" href="<?php echo $r['url']; ?>"><?php echo $r['url']; ?></a>
                              </div>
                            </div>
                          </div>

                    <?php } ?>

                </div>

            </div>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>


    <!-- unlock modal -->
    <div class="modal fade" id="unlock" tabindex="-1" aria-labelledby="modalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="modalLabel"><i class="fa fa-unlock"></i> Unlock Database</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>

          <form name="unlock_bookmarks" method="post" enctype="multipart/form-data">
              <div class="modal-body">
                  <div class="mb-3">
                    
                    <p><span class="badge bg-warning text-dark">Warning</span> -::- RSA private and public keys generation -::- <span class="badge bg-warning text-dark">Warning</span></p>
                    <p>Open a shell in your terminal and type the following commands:</p>
                    <code>$ openssl genrsa -out private.key 2048</code><br>
                    <code>$ openssl rsa -in private.key -out public.pem -outform PEM -pubout</code><br><br>
                    <p>Now you have your pair of keys, upload both to the server.</p><hr>
                    
                  </div>
                  <div class="mb-3">
                    <label for="file" class="col-form-label">KEY FILE</label>
                    <input type="file" class="form-control-file" name="file[]" id="file" multiple="multiple">
                  </div>
                  <!--
                  <div class="mb-3">
                    <label for="pass-phrase" class="col-form-label">PASS PHRASE <small>(if required)</small></label>
                    <input type="password" class="form-control" name="pass_phrase" id="pass-phrase">
                  </div>
                  -->
                  
                    <div id="preview"></div>

                    <div class="d-flex justify-content-center">
                        <div class="spinner-border" role="status" id="spinner_grow">
                            <span class="visually-hidden">Loading...</span>
                        </div>
                    </div>
                  
              </div>
              <div class="modal-footer">
                <input type="button" class="btn btn-success" id="btn_upload" value="Unlock Database">
              </div>

          </form>

        </div>
      </div>
    </div>



    <!-- lock modal -->
    <div class="modal fade" id="lock" tabindex="-1" aria-labelledby="modalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="modalLabel"><i class="fa fa-lock"></i> Lock Database</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
              <div class="mb-3">
                  <div id="preview-lock"></div>
                  <div class="d-flex justify-content-center">
                      <div class="spinner-border" role="status" id="locking">
                          <span class="visually-hidden">Locking..</span>
                      </div>
                  </div>
              </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
          </div>

        </div>
      </div>
    </div>




    <main class="container-fluid" style="position:relative;top:75px;">


        <?php if (isset($_SESSION["lock_state"]) and $_SESSION["lock_state"] === true) { ?>

          <div aria-live="polite" aria-atomic="true" class="d-flex justify-content-center align-items-center w-100">
            <div class="toast text-white bg-danger border-0" role="alert" aria-live="assertive" aria-atomic="true">
                <div class="d-flex">
                  <div class="toast-body">
                    <strong>Warn:</strong> Database is locked.
                  </div>
                  <button type="button" class="btn-close btn-close-white me-2 m-auto" data-bs-dismiss="toast" aria-label="Close"></button>
                </div>
            </div>
          </div>

        <?php } ?>


        <div>
            <!-- results here -->
            <ul id="searchResult"></ul>
            <div class="clear"></div>
            <div id="userDetail"></div>
        </div>
    </main>





    <footer class="fixed-bottom">
        <a href="http://www.freepik.com">The image was designed by pikisuperstar / Freepik</a>
    </footer>




<script src="assets/jquery.min.js"></script>
<!-- JavaScript Bundle with Popper -->
<script src="assets/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
<script src='assets/typeahead.bundle.min.js'></script>
<script src='assets/bootstrap-tagsinput.min.js'></script>
<!-- <script src='assets/bootstrap-treeview.js'></script> -->


<script>

$(document).ready(function(){

    $('#lock').on('show.bs.modal', function () {
        $("#locking").fadeIn(300);
        var locked = 1;

        $.ajax({
		    url: "lock_bookmarks.php",
		    type: "post",
		    cache:false,
		    //data: $('form#unlock-bookmarks').serialize(),
            data: {locked:locked},
            dataType: 'text',
            //contentType: false,
            //processData: false,
		    success: function(response){
                if (response != 0) {
                  // preview contains a message
                  $('#preview-lock').append(response);
                } else {
                  alert('error: database could not be locked.');
                }
			    //$("#unlock").html(response)
			    //$("#contact-modal").modal('hide');
                $("#locking").fadeOut(300);
		    }
        });

    });


    $("#spinner_grow").hide();
	$("#btn_upload").click(function(e){

	    // upload key
        var fd = new FormData();
        
        // Read selected files
        var totalfiles = document.getElementById('file').files.length;
        for (var index = 0; index < totalfiles; index++) {
            fd.append("file[]", document.getElementById('file').files[index]);
        }
        
        /* works for a single file
        var files = $('#file')[0].files[0];
        fd.append('file[]',files);
        */

        // ajax call
        $.ajax({
		    url: "unlock_bookmarks.php",
		    type: "post",
		    cache:false,
		    //data: $('form#unlock-bookmarks').serialize(),
            data: fd,
            contentType: false,
            processData: false,
		    success: function(response){

                if (response != 0) {
                  // preview contains a message
                  $('#preview').append(response);
                  $("#spinner_grow").fadeIn(300);
                } else {
                  alert('error: file upload failed.');
                }

			    //$("#unlock").html(response)
			    //$("#contact-modal").modal('hide');
			    //alert('passed here' + $("#unlock").html(response));
                $("#spinner_grow").fadeOut(300);
                $("#btn_upload").hide();
			    
		    }
        });
	    e.preventDefault();

	});
});


//var data = '[{ "value": 1, "text": "Task 1", "continent": "Task" }, { "value": 2, "text": "Task 2", "continent": "Task" }]';
var data = '<?php echo $tagsJSON; ?>';

//get data pass to json
var task = new Bloodhound({
  datumTokenizer: Bloodhound.tokenizers.obj.whitespace("text"),
  queryTokenizer: Bloodhound.tokenizers.whitespace,
  local: jQuery.parseJSON(data) //your can use json type
});
task.initialize();

var elt = $("#add-tags");
elt.tagsinput({
  itemValue: "value",
  itemText: "text",
  typeaheadjs: {
    name: "task",
    displayKey: "text",
    source: task.ttAdapter()
  }
});


//insert data to input in load page
elt.tagsinput("add", {
  value: 1,
  text: "General",
  continent: "Task"
});
</script>


<script>
$(document).ready(function(){

    $('.toast').toast('show');

    // logo popover
    $('[data-bs-toggle="popover"]').popover();
    
    $("#txt_search").keyup(function(){
        var search = $(this).val();
        if(search != ""){
            $.ajax({
                url: 'search_ajx.php',
                type: 'post',
                data: {search:search, type:1},
                dataType: 'json',
                success:function(response){
                    var len = response.length;
                    $("#searchResult").empty();
                    for( var i = 0; i<len; i++){
                        var id = response[i]['id'];
                        var url = response[i]['url'];
                        $("#searchResult").append("<li value='"+id+"'>"+url+"</li>");
                    }
                    // binding click event to li
                    $("#searchResult li").bind("click",function(){
                        setText(this);
                    });
                }
            });
        }
    });
});

// Set Text to search box and get details
function setText(element){
    var value = $(element).text();
    var id = $(element).val();
    $("#txt_search").val(value);
    $("#searchResult").empty();
    // Request User Details
    $.ajax({
        url: 'search_ajx.php',
        type: 'post',
        data: {id:id, type:2},
        dataType: 'json',
        success: function(response){
            var len = response.length;
            $("#userDetail").empty();
            if(len > 0){
                var id       = response[0]['id'];
                var url      = response[0]['url'];
                var metadata = response[0]['metadata'];
                var tags     = response[0]['tags'];
                $("#userDetail").append("<table class='table table-dark'><thead><tr><th scope='col'>#</th><th scope='col'>URL</th><th scope='col'>METADATA</th><th scope='col'>TAGS</th></tr></thead><tbody><tr><td>"+id+"</td><td><a target='_blank' href='"+url+"'>"+url+"</a></td><td> " + metadata + "</td><td><span class='badge rounded-pill bg-primary'>" + tags + "</span></td></tr></tbody></table>");
            }
        }
    });
}
</script>



<script type="text/javascript">
/* sample
$(document).ready(function(){
    $('#tree').treeview({
        data: getTree(),
        levels: 5,
        showTags: true
    });
});

function getTree() {

    // sample generate tree structure
    var tree = [
      {
        text: "Parent 1",
        href: '#parent1',
        tags: ['4'],
        nodes: [
          {
            text: "Child 1",
            href: '#child1',
            tags: ['2'],
            nodes: [
              {
                text: "Grandchild 1",
                href: '#grandchild1',
                tags: ['0']
              },
              {
                text: "Grandchild 2",
                href: '#grandchild2',
                tags: ['0']
              }
            ]
          },
          {
            text: "Child 2",
            href: '#child2',
            tags: ['0']
          }
        ]
      },
      {
        text: "Parent 2",
        href: '#parent2',
        tags: ['0']
      },
      {
        text: "Parent 3",
        href: '#parent3',
        tags: ['0']
      },
      {
        text: "Parent 4",
        href: '#parent4',
        tags: ['0']
      },
      {
        text: "Parent 5",
        href: '#parent5'  ,
        tags: ['0']
      }
    ];
    return data; // return data to get full list of tags
}
*/
</script>


</body>
</html>